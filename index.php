<?php
/**
 * Created by PhpStorm.
 * User: rglissmann
 * Date: 8/3/2017
 * Time: 11:46 AM
 */

include "Classes/membership.php";
include "Classes/MyFormErrors.php";
include "Classes/MyView.php";
include "Classes/Email.php";
require 'vendor/autoload.php';

$myView = new \memberships\MyView("Templates/");
$myView->render("header.inc");
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if ($_REQUEST["Submit"] == "SubmitInitial") {
        try {
            $member = new \memberships\member();
            $member->__set("FirstName", strtoupper(filter_var($_POST["FirstName"], FILTER_SANITIZE_STRING)));
            $member->__set("LastName", strtoupper(filter_var($_POST["LastName"], FILTER_SANITIZE_STRING)));
            $member->__set("DOB", strtoupper(filter_var($_POST["BirthDate"],FILTER_SANITIZE_STRING)));
            $api = new \memberships\NEAApi($member);
            if ($api->GetMemberInfo()) {
                //Display membership form
                $myView->vars = $member;
                $myView->render("body.inc");
                $myView->render("footer.inc");
            } else {
                //Display non-member message
                $myView->render("error.inc");
                $myView->render("footer.inc");
            }
        } catch (exception $ex) {
            echo $ex->getMessage();
        }
    }
    if($_REQUEST["Submit"] == "SubmitForm"){
        $Check = new \memberships\MyFormErrors();
        $Check->CheckForErrors($_POST);
        if ($Check->Error) {
            $variables = array();
            $variables["ErrorVars"] = $Check->ErrorVars;
            $variables["Member"] = $Check->member;
            $myView->vars = $variables;
            //$myView->vars = $Check->ErrorVars;
            //$myView->Member = $Check->member;

            $myView->render("body.inc");
            $myView->render("footer.inc");
        } else {
            $member = new \memberships\member();
            $member = $Check->member;
            if ($member->CreditCardCharge()) {
                $myView->vars = $member;
                //Show success message
                $myView->render("finish.inc");
                $myView->render("footer.inc");
            } else {
                $myView->vars = $member;
                $myView->render("body.inc");
                $myView->render("footer.inc");
            }
        }
    }
}
else{
    $myView->render("Intro.inc");
    $myView->render("footer.inc");
}

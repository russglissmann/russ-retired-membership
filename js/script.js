/**
 * Created by rglissmann on 2/1/2017.
 */

$(function ($) {
    if($("#Intro").length > 0) {
        $("#BirthDate").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
        $("#MPhone").mask("(999) 999-9999",{placeholder:"(xxx) xxx-xxxx"});
        $("#SSN4").mask("9999",{placeholder:"xxxx"});
        $("#RetiredRenewalStart").validate({
            debug: false,
            rules: {
                FirstName: "required",
                LastName: "required",
                BirthDate: "required",
                SSN4: {
                    number:true,
                    rangelength:[4,4]
                },
            },
            messages: {
                FirstName: "Please enter your first name.",
                LastName: "Please enter your last name.",
                BirthDate: "BirthDate is required",
                SSN4: {
                    rangelength: "SSN4 must be four digits.",
                    number:"SSN4 must only be numbers."
                }
            }
        });
    }
    else {
        var now = new Date();
        var year = now.getFullYear();
        var month = now.getMonth();
        var membershipYearStart = 0;
        var membershipYearEnd = 0;
        // JavaScript months are 0 based
        if(month >=8){
            membershipYearStart = year;
            membershipYearEnd = (year +1);
        }
        else{
            membershipYearEnd = year;
            membershipYearStart = (year -1);
        }
        $("#MembershipYearStart").html(membershipYearStart);
        $("#HIMembershipYearStart").val(membershipYearStart);
        $("#MembershipYearEnd").html(membershipYearEnd);
        $("#HIMembershipYearEnd").val(membershipYearEnd);
        $("#MembershipYearSpanStart").html(membershipYearStart);
        $("#MembershipYearSpanEnd").html(membershipYearEnd);
        $("#BirthDate").mask("99/99/9999", {placeholder: "mm/dd/yyyy"});
        $("#MPhone").mask("(999) 999-9999", {placeholder: "(xxx) xxx-xxxx"});
        $("#HZip").mask("99999?-9999", {placeholder: "xxxxx-xxxx"});
        $("#CCZip").mask("99999?-9999", {placeholder: "xxxxx-xxxx"});
        $("#CCExp").mask("9999", {placeholder: "mmyy"});
        $("#SubmitForm").prop("disabled", true);
        $("#SubmitForm").addClass("SubmitButtonInactive");
        $("#IAgree").click(function () {
            if ($("#IAgree").prop("checked")) {
                $("#SubmitForm").prop("disabled", false);
                $("#SubmitForm").removeClass("SubmitButtonInactive").addClass("SubmitButtonActive");
            }
            ;
        });
        $("#CCNumber").val("");
        $("#CCCVV").val("");
        if ($("#HICCError").val() !== "") {
            $("#CCError").html($("#HICCError").val());
        }
        $('#IniSubmit').on('click', function() {
            alert('Clicked');
            var $this = $(this);
            $this.button('loading');
        });
        $('input[type="radio"]').on('change', (function () {
            if($("#ESPY").is(":checked") || $("#ESPN").is(":checked")) {
                if ($("#LifetimeYes").is(":checked") || $("#LifetimeNo").is(":checked")) {
                    var rname = $(this).attr('name');
                    var xmlhttp = new XMLHttpRequest();
                    var LocalID = $("#LocalID").val();
                    if ($("#ESPY").is(":checked")) {
                        var MemberType = "CLASSIFIED";
                    }
                    else {
                        var MemberType = "CERTIFIED";
                    }
                    if ($("#LifetimeYes").is(":checked")) {
                        var MemberTypeCode = "RT-8";
                    }
                    else {
                        var MemberTypeCode = "RT-7"
                    }

                    xmlhttp.onreadystatechange = function () {
                        if (this.readyState == 4 && this.status == 200) {
                            myObj = JSON.parse(this.responseText);
                            $("#NEADues").html(myObj.NEA);
                            $("#HINEADues").val(myObj.NEA);
                            $("#CEADues").html(myObj.CEA);
                            $("#HICEADues").val(myObj.CEA);
                            $("#LocalDues").html(myObj.Local);
                            $("#HILocalDues").val(myObj.Local);
                            $("#SumDues").html(parseFloat(myObj.NEA) + parseFloat(myObj.CEA) + parseFloat(myObj.Local) + ".00");
                            $("#sumYearlyDuesCC").html($("#SumDues").html());
                            $("#HICCAmount").val($("#SumDues").html());
                        }
                    };
                    var src = "http://membership.local/GetDues.php?MemberTypeCode=" + MemberTypeCode + "&LocalID=" + LocalID + "&MemberType=" + MemberType;
                    xmlhttp.open("GET", src, true);
                    xmlhttp.send();
                }
            }
        }));
        // Start of validation add a method for determining expiration date
        jQuery.validator.addMethod("ExpDate", function (value, element) {
            var CCMonth = parseInt(value.slice(0, 2)); //Get the month part, first two digits
            var CCYear = parseInt(value.slice(2, 4)); //Get the year part, last two digits
            var validMonth = false; //Boolean if the month is valid, default false
            var validYear = false; //Boolean if year is valid, default false
            var thisYear = new Date().getFullYear(); //Get this year value
            var thisMonth = new Date().getMonth(); //Get this month
            var twoDigitYear = parseInt(thisYear.toString().slice(2, 4));
            if (CCMonth >= 1 && CCMonth <= 12) { //Test the month, has to be greater than, equal to 1, but not more than 12
                validMonth = true; //If it meets that criteria, then the month is valid
            }
            if (CCYear >= twoDigitYear) { //Test the year, has to be equal to, or greater than this year
                if (CCYear == twoDigitYear) {
                    if (CCMonth > thisMonth) {
                        validYear = true;
                    }
                    else {
                        validYear = false;
                    }
                }
                else {
                    validYear = true;
                }

            }
            if (validMonth && validYear) {
                return true;
            }
            else {
                return false;
            }
        });

        // Validate the form
        $("#RetiredRenewal").validate({
            debug: false,
            submitHandler: function (form) {
                var response = grecaptcha.getResponse();
                //recaptcha failed validation
                if (response.length == 0) {
                    $('#CaptchaError').show();
                    $("#CaptchaError").text("Captcha box unchecked");
                    return false;
                }
                //recaptcha passed validation
                else {
                    $('#CaptchaError').hide();
                    return true;
                }
            },
            rules: {
                FirstName: "required",
                LastName: "required",
                HAddress: "required",
                HCity: "required",
                HState: "required",
                HZip: "required",
                BirthDate: "required",
                SSN4: {
                    number:true,
                    rangelength:[4,4]
                },
                HEmail: {
                    required: true,
                    email: true
                },
                Lifetime:{
                    required: function () {
                        return (!$("#ESPN").is(":checked") || !$("#ESPY").is(":checked"));
                    }
                },
                ESP:{
                    required:function () {
                        return (!$("#LifetimeNo").is(":checked") || !$("#LifetimeYes").is(":checked"));
                    }
                },
                CCFirstName: "required",
                CCLastName: "required",
                CCAddress:  "required",
                CCCity: "required",
                CCSt:  "required",
                CCZip:  "required",
                CCNumber:  {
                    required: true,
                    creditcard: true
                },
                CCCVV: "required",
                CCExp: "required",
                IAgree: "required"
            },
            messages: {
                FirstName: "Please enter your first name.",
                LastName: "Please enter your last name.",
                HAddress: "Please enter your street address",
                HCity: "City is required",
                HState: "State is required",
                HZip: "Zip code is required",
                BirthDate: "BirthDate is required",
                SSN4: {
                    rangelength: "SSN4 must be four digits.",
                    number:"SSN4 must only be numbers."
                },
                HEmail: "Home Email is required to send your copy of the membership application.",
                Lifetime:{
                    required: "You must select whether or not you want a retired lifetime membership"
                },
                ESP:{
                    required: "You must select whether you were an ESP"
                },
                CCFirstName: "You must enter the first name on the credit card",
                CCLastName: "You must enter the last name on the credit card",
                CCAddress: "You must enter the billing address for the credit card",
                CCCity: "You must enter the billing city for the credit card",
                CCSt: "You must enter the billing state for the credit card",
                CCZip: {
                    required: "You must enter the billing zip code for the credit card",
                    zipcodeUS: "Your zip code is incorrect"
                },
                CCNumber: {
                    required: "You must enter the credit card number",
                    creditcard: "Your card number doesn't appear to be correct"
                },
                CCCVV: {
                    required: "You must enter the CVV from your credit card",
                    maxLength: "Your CVV is too long",
                    number: "Your CVV can only be numeric"
                },
                CCExp: {
                    required: "You must enter the credit card expiration date",
                    ExpDate: "Your credit card expiration date is incorrect"
                },
                IAgree: "You must agree to the membership terms before continuing"

            }
        });
    }



});




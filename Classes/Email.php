<?php
/**
 * Created by PhpStorm.
 * User: rglissmann
 * Date: 8/31/2017
 * Time: 11:46 AM
 */

namespace memberships;

// Mailgun email from Composer
use Mailgun\Mailgun;

class Email
{
    private $api_key = 'key-3-kjo7axk-lzno11mmrs3dz8skl1t-n3';
    private $domain = "mg.coloradoea.org";
    private $member;

    function __construct($member)
    {
        $this->member = $member;
    }

    function sendSuccess()
    {
        // Create the email message
        $text = "<strong>Thank you</strong> for renewing your retired membership with Colorado Education Association. We have received your payment of $" . $this->member->TotalDues . " billed on your credit card ending in " . substr($this->member->CCNumber, -4) .".</br>";
        $text .= "We have the following as your personal information:</br><ul>";
        $text .= "<li>First Name: ". $this->member->FirstName . "</li>";
        $text .= "<li>Middle Initial: " . $this->member->MI . "</li>";
        $text .= "<li>Last Name: " . $this->member->LastName . "</li>";
        $text .= "<li>Date of birth: " . $this->member->DOB . "</li>";
        $text .= "<li>Address: " .  $this->member->HAddress . "</li>";
        $text .= "<li>City: " .$this->member->HCity . "</li>";
        $text .= "<li>State: " . $this->member->HState . "</li>";
        $text .= "<li>Mobile Phone: " . $this->member->MPhone . "</li>";
        $text .= "<li>Email: " . $this->member->HEmail . "</li>";
        $text .= "</ul>";
        $text .= "<h2><strong>Member Agreement</strong></h2>";
        $text .= "<p>Dues payments are not deductible as charitable contributions for federal income tax purposes. Dues payments (or a portion) may be deducted as a miscellaneous itemized deduction.</p>";
        $text .= "<p>I authorize payment of dues in the sum determined by my local affiliate (if applicable), the Colorado and National Education Associations.</p>";
        //Lifetime
        if($this->member->Lifetime == "Yes"){
            $text .= "<p>This form is for your Lifetime membership.</p>";
        }
        //Annual
        else{
            $text .= "<p>This form is for the " . $this->member->MembershipYearStart . " - " . $this->member->MembershipYearEnd . " membership year: September 1, " . $this->member->MembershipYearStart . " through
                        August 31, " . $this->member->MembershipYearEnd . " (12 months). </p>";
        }


        // Email subject
        $subject = "Retired membership renewal for CEA";

        // Create the Mailgun object from the API
        $mg = Mailgun::create($this->api_key);

        //Send the message
        $result = $mg->messages()->send($this->domain, ['from' => 'Retired Membership <postmaster@coloradoea.org>',
                                                'to' => $this->member->HEmail,
                                                'bcc' => 'rglissmann@coloradoea.org',
                                                'subject' => $subject,
                                                'html' => $text]);

    }

    function sendConfirmatio(){
        //create the email message
        $text = "<h2>CEA Retired Membership Renewal for " . $this->member->MembershipYearStart . "/" . $this->member->MembershipYearEnd ."</h2>";
        $text .= "Here is the membership information:<ul>";
        $text .= "<li>First Name: ". $this->member->FirstName . "</li>";
        $text .= "<li>Middle Initial: " . $this->member->MI . "</li>";
        $text .= "<li>Last Name: " . $this->member->LastName . "</li>";
        $text .= "<li>Date of birth: " . $this->member->DOB . "</li>";
        $text .= "<li>Address: " .  $this->member->HAddress . "</li>";
        $text .= "<li>City: " .$this->member->HCity . "</li>";
        $text .= "<li>State: " . $this->member->HState . "</li>";
        $text .= "<li>Mobile Phone: " . $this->member->MPhone . "</li>";
        $text .= "<li>Email: " . $this->member->HEmail . "</li>";
        $text .= "<li>Amount paid: $" . $this->member->TotalDues . "</li>";
        $text .= "<li>Auth Code: " . $this->member->authCode . "</li>";
        $text .= "<li>Local: " . $this->member->Local . "</li>";
        $text .= "<li>Member Type: " . $this->member->MemberType ."</li>";
        $text .= "<li>Lifetime: " . $this->member->Lifetime ."</li>";
        $text .= "<li>ESP: " . $this->member->ESP . "</li>";
        $text .= "<li>NEA Dues: $" . $this->member->NEADues . "</li>";
        $text .= "<li>CEA Dues: $" . $this->member->CEADues . "</li>";
        $text .= "<li>Local Dues: $" . $this->member->LocalDues . "</li>";
        $text .= "<li>Last four CC Number: " . substr($this->member->CCNumber, -4) ."</li>";
        $text .= "<li>Expiration Date: " . $this->member->CCExp ."</li></ul>";
        // Email subject
        $subject = "Retired membership renewal for CEA";

        // Create the Mailgun object from the API
        $mg = Mailgun::create($this->api_key);

        //Send the message
        $result = $mg->messages()->send($this->domain, ['from' => 'Retired Membership <postmaster@coloradoea.org>',
            'to' => 'kbenjamin@coloradoea.org',
            'bcc' => 'rglissmann@coloradoea.org',
            'subject' => $subject,
            'html' => $text]);
    }
}
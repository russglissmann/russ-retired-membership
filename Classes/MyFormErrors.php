<?php
/**
 * Created by PhpStorm.
 * User: rglissmann
 * Date: 8/23/2017
 * Time: 10:02 AM
 */

namespace memberships;
use ReCaptcha\ReCaptcha;


class MyFormErrors
{
    private $ErrorVars = array();
    private $member;
    private $Error;
    private $Secret = '6LfZ6ioUAAAAAC4tWirXMEG_6wnhliJNS1aAtLSD';


    function __get($name)
    {
        return $this -> $name;
    }

    function CheckForErrors($form){
        $this->member = new member();
        // If the form submission includes the "g-captcha-response" field
        // Create an instance of the service using your secret
        $recaptcha = new ReCaptcha($this->Secret);
        $resp = $recaptcha->verify($form['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
        if ($resp->isSuccess()){
            foreach ($form as $key => $value) {
                switch ($key) {
                    case "DOB":
                        preg_match("/\d{2}(\/|-)\d{2}(\/|-)\d{4}/", $value, $output_array);
                        if (!$output_array[0]) {
                            $this->ErrorVars["BirthDateError"] = "There is a problem with your date of birth.";
                            $this->Error = true;
                        } else {
                            $this->member->__set("DOB", $value);
                        }
                        break;
                    case "SSN4":
                        if (filter_var($_POST["SSN4"], FILTER_SANITIZE_NUMBER_INT) == "" || strlen($_POST["SSN4"]) != 4) {
                            $this->ErrorVars["SSN4Error"] = "There is a problem with your SSN4.";
                            $this->Error = true;
                        } else {
                            $this->member->__set("SSN4", $value);
                        }
                        break;
                    case "HZip":
                        //Parse the zipcode for -
                        $ZipCode = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
                        $ZipCodes = explode("-", $value);
                        //Look to see that there is a zip code
                        if ($ZipCodes[0]) {
                            $ZipTemp = filter_var($ZipCodes[0], FILTER_SANITIZE_NUMBER_INT);
                            //Check to see that it is a number and that the length is 4
                            if ($ZipTemp == "" || strlen($ZipTemp) != 5) {
                                $this->ErrorVars["ZipCodeError"] = "There is a problem with your Zip code.";
                                $this->Error = true;
                            } else {
                                $this->member->__set("HZip", $ZipCode);
                            }
                        } //Check to see that the second part of the zip is a number
                        elseif (isset($ZipCodes[1])) {
                            if (!filter_var($ZipCodes[1], FILTER_SANITIZE_NUMBER_INT && strlen($ZipCodes[1]) != 4)) {
                                $this->ErrorVars["ZipCodeError"] = "There is a problem with your Zip code.";
                                $this->Error = true;
                            } else {
                                $this->member->__set("HZip", $ZipCode);
                            }
                        }
                        break;
                    case "MPhone":
                        if (filter_var($value, FILTER_SANITIZE_STRING) == "") {
                            $this->ErrorVars["MPhoneError"] = "There is a problem with your Mobile Phone.";
                            $this->Error = true;
                        } elseif (strlen($value) != 14) {
                            $this->ErrorVars["MPhoneError"] = "There is a problem with your Mobile Phone.";
                            $this->Error = true;
                        }
                        if (!filter_var($value, FILTER_SANITIZE_NUMBER_INT)) {
                            $this->ErrorVars["MPhoneError"] = "There is a problem with your mobile phone number.";
                            $this->Error = true;
                        } else {
                            $this->member->__set("MPhone", $value);
                        }
                        break;
                    case "HEmail":
                        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                            $this->ErrorVars["HomeEmailError"] = "There is an error with your home email address.";
                            $this->Error = true;
                        } else {
                            $this->member->__set("HEmail", $value);
                        }
                        break;
                    case "CCFirstName":
                        if (filter_var($value, FILTER_SANITIZE_STRING) == "") {
                            $this->ErrorVars["CCFirstNameError"] = "There is a problem with the credit card First Name.";
                            $this->Error = true;
                        } else {
                            $this->member->__set("CCFirstName", $value);
                        }
                        break;
                    case "CCLastName":
                        if (filter_var($value, FILTER_SANITIZE_STRING) == "") {
                            $this->ErrorVars["CCLastNameError"] = "There is a problem with the credit card Last Name.";
                            $this->Error = true;
                        } else {
                            $this->member->__set("CCLastName", $value);
                        }
                        break;
                    case "CCAddress":
                        if (filter_var($value, FILTER_SANITIZE_STRING) == "") {
                            $this->ErrorVars["CCAddressError"] = "There is a problem with the credit card address.";
                            $this->Error = true;
                        } else {
                            $this->member->__set("CCAddress", $value);
                        }
                        break;
                    case "CCCity":
                        if (filter_var($value, FILTER_SANITIZE_STRING) == "") {
                            $this->ErrorVars["CCCityError"] = "There is a problem with the credit card city.";
                            $this->Error = true;
                        } else {
                            $this->member->__set("CCCity", $value);
                        }
                        break;
                    case "CCZip";
                        if (filter_var($value, FILTER_SANITIZE_NUMBER_INT) == "") {
                            $this->ErrorVars["CCZipCodeError"] = "There is a problem with the credit card Zip Code.";
                            $this->Error = true;
                        } else {
                            //Parse the zipcode for -
                            $CCZipCodes = explode("-", $value);
                            //Look to see that there is a credit card zip code
                            if ($CCZipCodes[0]) {
                                $ZipTemp = filter_var($CCZipCodes[0], FILTER_SANITIZE_NUMBER_INT);
                                //Check to see that it is a number and that the length is 4
                                if ($ZipTemp == "" || strlen($ZipTemp) != 5) {
                                    $this->ErrorVars["CCZipCodeError"] = "There is a problem with your credit card Zip code.";
                                    $this->Error = true;
                                }
                            }

                            //Check to see that the second part of the zip is a number
                            if (isset($CCZipCodes[1])) {
                                if (filter_var($CCZipCodes[1], FILTER_SANITIZE_NUMBER_INT && strlen($CCZipCodes[1]) != 4)) {
                                    $this->ErrorVars["CCZipCodeError"] = "There is a problem with your credit card Zip code.";
                                    $this->Error = true;
                                }
                            }
                            $this->member->__set("CCZip", $value);
                        }
                        break;
                    case "CCSt":
                        if (filter_var($value, FILTER_SANITIZE_STRING) == "") {
                            $this->ErrorVars["CCStError"] = "There is a problem with your credit card state.";
                            $this->Error = true;
                        } else {
                            $this->member->__set("CCSt", $value);
                        }
                        break;
                    case "CCNumber":
                        $CCNumber = filter_var($_POST["CCNumber"], FILTER_SANITIZE_NUMBER_INT);
                        if (strlen($CCNumber) != 16) {
                            $this->ErrorVars["CCNumberError"] = "There is a problem with your credit card number.";
                            $this->Error = true;
                        } else {
                            $this->member->__set("CCNumber", $CCNumber);
                        }
                        break;
                    case "CCCVV":
                        $CCCVV = filter_var($_POST["CCCVV"], FILTER_SANITIZE_NUMBER_INT);
                        if (strlen($CCCVV) > 4) {
                            $this->ErrorVars["CCCVVError"] = "There is a problem with the credit card CVV number.";
                            $this->Error = true;
                        } else {
                            $this->member->__set("CCCVV", $value);
                        }
                        break;
                    case "CCExp":
                        $CCExp = filter_var($_POST["CCExp"], FILTER_SANITIZE_NUMBER_INT);
                        if (strlen($CCExp) != 4) {
                            $this->ErrorVars["CCExpError"] = "There is a problem with the credit card expiration date.";
                            $this->Error = true;
                        } else {
                            $this->member->__set("CCExp", $CCExp);
                        }
                        $DuesAmount = number_format($_POST["HICCAmount"]);
                        $this->member->__set("TotalDues", $DuesAmount);
                        break;
                    case "FirstName":
                        if (filter_var($value, FILTER_SANITIZE_STRING) == "") {
                            $this->ErrorVars["FirstNameError"] = "There is a problem with your First Name.";
                            $this->Error = true;
                        } else {
                            $this->member->__set("FirstName", $value);
                        }
                        break;
                    case "LastName":
                        if (filter_var($value, FILTER_SANITIZE_STRING) == "") {
                            $this->ErrorVars["LastNameError"] = "There is a problem with your First Name.";
                            $this->Error = true;
                        } else {
                            $this->member->__set("LastName", $value);
                        }
                        break;
                    case "MI":
                        $MI = filter_var($_POST["MI"], FILTER_SANITIZE_STRING);
                        $this->member->__set("MI", $MI);
                        break;
                    case "HAddress":
                        if (filter_var($value, FILTER_SANITIZE_STRING) == "") {
                            $this->ErrorVars["AddressError"] = "There is a problem with your home address.";
                            $this->Error = true;
                        } else {
                            $this->member->__set("HAddress", $value);
                        }
                        break;
                    case "HCity":
                        $City = filter_var($_POST["HCity"], FILTER_SANITIZE_STRING);
                        if (filter_var($value, FILTER_SANITIZE_STRING) == "") {
                            $this->ErrorVars["CityError"] = "There is a problem with your City.";
                            $this->Error = true;
                        } else {
                            $this->member->__set("HCity", $value);
                        }
                        break;
                    case "HState":
                        if (filter_var($value, FILTER_SANITIZE_STRING) == "" || strlen($value) != 2) {
                            $this->ErrorVars["StateError"] = "There is a problem with your state code.";
                            $this->Error = true;
                        } else {
                            $this->member->__set("HState", $value);
                        }
                        break;
                    case "Local":
                        $this->member->__set("Local", $value);
                        break;
                    case "LocalID":
                        $this->member->__set("LocalID", $value);
                        break;
                    case "NEADues":
                        $this->member->__set("NEADues", $value);
                        break;
                    case "CEADues":
                        $this->member->__set("CEADues", $value);
                        break;
                    case "LocalDues":
                        $this->member->__set("LocalDues", $value);
                        break;
                    case "HICCAmount":
                        $this->member->__set("TotalDues", $value);
                        break;
                    case "InaID":
                        $this->member->__set("InaID", $value);
                        break;
                    case "MemberType":
                        $this->member->__set("MemberType", $value);
                        break;
                    case "Lifetime":
                        $this->member->__set("Lifetime", $value);
                        break;
                    case "ESP":
                        $this->member->__set("ESP", $value);
                        break;
                    case "HApt":
                        $this->member->__set("HApt", $value);
                        break;
                    case "HIMembershipYearStart":
                        $this->member->__set("MembershipYearStart", $value);
                        break;
                    case "HIMembershipYearEnd":
                        $this->member->__set("MembershipYearEnd", $value);
                        break;
                }
            }
        }
        else{
            foreach ($resp->getErrorCodes() as $code) {
                $ErrorVars["CaptchaError"] = $code;
            }
            $this->Error = true;
        }
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: rglissmann
 * Date: 8/8/2017
 * Time: 11:23 AM
 */

namespace memberships;

class MyView {
    protected $template_dir = 'templates/';
    protected $vars = array();
    public function __construct($template_dir = null) {
        if ($template_dir !== null) {
            // Check here whether this directory really exists
            $this->template_dir = $template_dir;
        }
    }
    public function render($template_file) {
        try {
            if (file_exists($this->template_dir . $template_file)) {
                include $this->template_dir . $template_file;
            } else {
                throw new Exception('no template file ' . $template_file . ' present in directory ' . $this->template_dir);
            }
        }
        catch (exception $ex){
            echo $ex;
        }
    }

    public function __set($name, $value) {
        $this->vars[$name] = $value;
    }
    public function __get($name) {
        return $this->vars[$name];
    }
}
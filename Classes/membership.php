<?php
/**
 * Created by PhpStorm.
 * User: rglissmann
 * Date: 8/2/2017
 * Time: 2:24 PM
 */


namespace memberships;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;



class member
{

    // Constant
    const RESPONSE_OK = "Ok";

    // properties
    private $FirstName;
    private $LastName;
    private $MI;
    private $DOB;
    private $SSN4;
    private $HAddress;
    private $HApt;
    private $HCity;
    private $HZip;
    private $HState;
    private $MPhone;
    private $HEmail;
    private $Local;
    private $LocalID;
    private $NEADues;
    private $CEADues;
    private $LocalDues;
    private $TotalDues;
    private $MemberType;
    private $InaID;
    private $CCFirstName;
    private $CCLastName;
    private $CCAddress;
    private $CCCity;
    private $CCZip;
    private $CCSt;
    private $CCExp;
    private $CCCVV;
    private $CCNumber;
    private $Lifetime;
    private $ESP;
    private $CCErrorCode;
    private $CCErrorMessage;
    private $authCode;
    private $CCMessage;
    private $CCChargeDescription;
    private $MembershipYearStart;
    private $MembershipYearEnd;

    //Getters & Setters Methods

    function __get($name)
    {
        return $this -> $name;
    }

    function __set($name, $value)
    {
        $this->$name = $value;
    }

    function CreditCardCharge()
    {
        // Common setup for API credentials
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName("76X9dFw5");
        $merchantAuthentication->setTransactionKey("4a4HSuY35S8Fhm3p");

        // Create the payment data for a credit card
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($this->CCNumber);
        $creditCard->setExpirationDate($this->CCExp);
        $creditCard->setCardCode($this->CCCVV);

        //Create the address
        $address = new AnetAPI\CustomerAddressType();
        $address->setAddress($this->CCAddress);
        $address->setCity($this->CCCity);
        $address->setState($this->CCSt);
        $address->setZip($this->CCZip);
        $address->setFirstName($this->CCFirstName);
        $address->setLastName($this->CCLastName);
        $address->setEmail($this->HEmail);

        //Create the payment type, in this case credit card
        $paymentOne = new AnetAPI\PaymentType();
        $paymentOne->setCreditCard($creditCard);

        // Create an order
        $order = new AnetAPI\OrderType();
        $order->setDescription("CEA Retired Membership Dues ID# " . $this->InaID);

        // Create a transaction
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType('authCaptureTransaction');
        $transactionRequestType->setAmount($this->TotalDues);
        $transactionRequestType->setPayment($paymentOne);
        $transactionRequestType->setOrder($order);
        $transactionRequestType->setBillTo($address);

        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setTransactionRequest($transactionRequestType);
        $controller = new AnetController\CreateTransactionController($request);
        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);

        if ($response != null) {
            // Check to see if the API request was successfully received and acted upon
            if ($response->getMessages()->getResultCode() == $this::RESPONSE_OK) {
                // Since the API request was successful, look for a transaction response
                // and parse it to display the results of authorizing the card
                $tresponse = $response->getTransactionResponse();
                if ($tresponse != null && $tresponse->getMessages() != null) {
                    $this->authCode = $tresponse->getAuthCode();
                    $this->CCMessage = $tresponse->getMessages()[0]->getCode();
                    $this->CCChargeDescription = $tresponse->getMessages()[0]->getDescription();
                    $email = new Email($this);
                    $email->sendSuccess();
                    $email->sendConfirmatio();
                    $db = new DBFunctions();
                    $db->StoreMembership($this);
                    return true;
                } else {
                    if ($tresponse->getErrors() != null) {
                        $this->CCErrorCode = $tresponse->getErrors()[0]->getErrorCode();
                        $this->CCErrorMessage = $tresponse->getErrors()[0]->getErrorText();
                        $this->CCChargeDescription = $tresponse->getMessages;
                        echo("Transaction failed: " . $tresponse->getErrors()[0]->getErrorText());
                        return false;
                    } else {
                        //Transaction failed
                        if ($tresponse->getErrors() != null) {
                            $this->CCErrorCode = $tresponse->getErrors()[0]->getErrorCode();
                            $this->CCErrorMessage = $tresponse->getErrors()[0]->getErrorText();
                            echo("Transaction failed: " . $tresponse->getErrors()[0]->getErrorText());
                            return false;
                        }
                    }
                }
            } else {
                // Transaction failed
                $tresponse = $response->getTransactionResponse();

                if ($tresponse != null && $tresponse->getErrors() != null) {
                    $this->CCErrorCode = $tresponse->getErrors()[0]->getErrorCode();
                    $this->CCErrorMessage = $tresponse->getErrors()[0]->getErrorText();
                    echo("Transaction failed: " . $tresponse->getErrors()[0]->getErrorText());
                    return false;
                } else {
                    $this->CCErrorCode = $response->getMessages()->getMessage()[0]->getCode();
                    $this->CCErrorMessage = $response->getMessages()->getMessage()[0]->getText();
                    echo("Transaction failed: " . $tresponse->getErrors()[0]->getErrorText());
                    return false;
                }
            }
        } else {
            $this->CCErrorMessage = "No response returned";
            echo("Transaction failed: No response returned");
            return false;
        }
    }
}


class DBFunctions{
    private $servername = "localhost";
    private $username = "cea";
    private $password = "psUDickyAdVentI";
    private $dbname = "Memberships";

    function GetAPIVars(){
        $Token = "";
        $StrSQL = "select * from nea_token";
        try {
            $conn = new \PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $strsql = trim($StrSQL);
            $apiVars = array();
            foreach($conn->query($strsql) as $row){
                $apiVars['appId'] = $row['appId'];
                $apiVars['appTokenId'] = $row['appTokenId'];
                $apiVars['appUserKey'] = $row['appUserKey'];
                $apiVars['appPwd'] = $row['appPwd'];
            }
            return $apiVars;
        }
        catch(PDOException $e){
            echo "PDO Error: " . $e->getMessage();
        }
        return $Token;
    }

    function SetAPIToken($Token){
        try{
            $strsql = "update nea_token set appTokenId = :apTokenID";
            $conn = new \PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $stmt = $conn -> prepare($strsql);
            $stmt -> bindValue(':apTokenID', $Token);
            $stmt -> execute();
        }
        catch(PDOException $e){
            echo "PDO Error: " . $e->getMessage();
        }
    }

    function StoreMembership($Member){
        try {
            $strsql = "insert into retiredmembership (MemberType, local, FirstName, LastName, MI, DOB, SSN4, Address, Apt, City, State, ZipCode, MobilePhone, HomeEmail, ";
            $strsql .= "CCN4, CCExp, CCApprovalCode, AmountPaid, Lifetime, ESP, MembershipYear) values (:MemberType, :local, :FirstName, :LastName, :MI, :DOB, :SSN4, :Address, :Apt, :City, :State, :ZipCode, :MobilePhone,";
            $strsql .= " :HomeEmail, :CCN4, :CCExp, :CCApprovalCode, :AmountPaid, :Lifetime, :ESP, :MembershipYear)";

            $conn = new \PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $strsql = trim($strsql);
            $stmt = $conn->prepare($strsql);

            $stmt->bindValue(':MemberType', $Member->MemberType);
            $stmt->bindValue(':local', $Member->Local);
            $stmt->bindValue(':FirstName', $Member->FirstName);
            $stmt->bindValue(':LastName', $Member->LastName);
            $stmt->bindValue(':MI', $Member->MI);
            $stmt->bindValue(':DOB', $Member->DOB);
            $stmt->bindValue(':SSN4', $Member->SSN4);
            $stmt->bindValue(':Address', $Member->HAddress);
            $stmt->bindValue(':Apt', $Member->HApt);
            $stmt->bindValue(':City', $Member->HCity);
            $stmt->bindValue(':State', $Member->HState);
            $stmt->bindValue(':ZipCode', $Member->HZip);
            $stmt->bindValue(':MobilePhone', $Member->MPhone);
            $stmt->bindValue(':HomeEmail', $Member->HEmail);
            $stmt->bindValue(':CCN4', substr($Member->CCNumber,-4));
            $stmt->bindValue(':CCExp', $Member->CCExp);
            $stmt->bindValue(':CCApprovalCode', $Member->authCode);
            $stmt->bindValue(':AmountPaid', $Member->TotalDues);
            $stmt->bindValue(':Lifetime', $Member->Lifetime);
            $stmt->bindValue(':ESP', $Member->ESP);
            $stmt->bindValue(':MembershipYear', $Member->MembershipYearStart . "/" . $Member->MembershipYearEnd);

            $stmt->execute();
            return true;
        }
        catch (\PDOException $e) {
            echo "PDO Error: " . $e->getMessage();
            return false;
        }
    }
}

class NEAApi{
    private $appId = "";
    private $appTokenId = "";
    private $appUserKey = "";
    private $appPwd = "";
    private $Member;
    private $ErrorMessage;

    function SetNEAApiVars(){
        $Data = new DBFunctions();
        $APIVars = $Data->GetAPIVars();
        $this->appId = $APIVars['appId'];
        $this->appTokenId = $APIVars['appTokenId'];
        $this->appUserKey = $APIVars['appUserKey'];
        $this->appPwd = $APIVars['appPwd'];
    }

    function SetNEAApiToken($Token){
        $Data = new DBFunctions();
        $Data->SetAPIToken($Token);
        $this->appTokenId = $Token;
    }

    function __construct(Member $Member)
    {
        $this->Member = $Member;
        $this->SetNEAApiVars();
    }

    function SetErrorMessage($Message){
        $this->ErrorMessage = $Message;
    }

    // Recursive function to retrieve membership information
    // Only time the function actually is recursive is if the NEA token has expired
    function GetMemberInfo(){
        // Retrieve the member array from the NEA API
        $MemberPersonalInfo = $this->CheckForMembership($this->Member->__get("FirstName"),$this->Member->__get("LastName"),$this->Member->__get("DOB"), $this->Member->__get("SSN4"));
        if(array_key_exists('TOKENv_ERROR_CODE', $MemberPersonalInfo)) {
            // Re-issue and save the token
            if(!$this->ResetNEAToken()){
                //Return and send the error message and display an error
                throw new \Exception("$this->ErrorMessage");
            }
            else{
                // Okay, the Token has been reset, so let's start over
                $this->GetMemberInfo();
            }
        }
        elseif(empty($MemberPersonalInfo)){
            //Non member
            return false;
        }
        else{
            //This is a member so now get the member type to make sure this is a retired individual
            $IsCO = false; // Default not a CO member since the API does not return only Colorado members
            foreach ($MemberPersonalInfo as $item) {
                //Retrieve the Membership information for each member using their InaID
                $Membinfo = $this->RetrieveMemberInfo($item['indvId']);
                foreach ($Membinfo as $info) {
                    //We only want CO retired members, as it is possible to have someone with the same name in another state that is also retired
                    switch ($info['uniservIntOrgId']) {
                        case "0000002894": //Pikes Peak Uniserv
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000002898": //Southeast
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000002902"://Spanish Peaks
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000002905": //Northern Colorado
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000002908": //Northeastern
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000002912": //San Luis
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000002921": //Ski Country
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000002927": //Front Range
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000002931": //Aurora Littleton
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000002934": //San Juan
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000002940": //Boulder Westminster
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000002944": //Central Adams
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000002970": //Colorado Springs
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000002976": //West Central
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000002985": //Denver
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000003048": //JCEA
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000003103": //Two Rivers
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000003176": //County Misc
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000003211": //NEA Life
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0000024257": //Unassigned
                            if (substr($info["neaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0007722035": //JESPA
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                        case "0007836034": //Big Thompson
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                            $this->Member->__set("Local", $info["localName"]);
                            $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                            $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                            $IsCO = true;
                            }
                            break;
                        case "0000683204": //Unassigned
                            if (substr($info["seaMembType"], 0, 2) == "RT") {
                                $this->Member->__set("Local", $info["localName"]);
                                $this->Member->__set("LocalID", $info["leaIntOrgId"]);
                                $this->Member->__set("MemberType", substr($info["seaMembType"], 0,4) );
                                $IsCO = true;
                            }
                            break;
                    }
                    if ($IsCO) { // Since this is a Colorado member set all necessary variables from the API
                        $this->Member->__set("SSN4", $item['ssn4']);
                        $this->Member->__set("MI", strtoupper($item['middleNm']));
                        $this->Member->__set("HAddress", strtoupper($item['addresses'][0]['addressLine2']));
                        $this->Member->__set("HCity", strtoupper($item['addresses'][0]['city']));
                        $this->Member->__set("HState", strtoupper($item['addresses'][0]['state']));
                        $this->Member->__set("HZip", strtoupper($item['addresses'][0]['formattedZip']));
                        $this->Member->__set("MPhone", strtoupper($item['phoneNumbers'][0]['formattedPhoneNumTxt']));
                        $this->Member->__set("HEmail", strtoupper($item['emailAddresses'][0]['emailAddressTxt']));
                        $this->Member->__set("InaID", strtoupper($item["indvId"]));
                        break; //Break $Membinfo out of the Foreach loop, we have found the membership
                    }
                }
                if($IsCO){
                    break; //Break out of $MemberPersonalInfo loop, we have our member
                }
            }
            if($IsCO){
                return true; //Function completed successfully
            }
            else{
                return false; //Function didn't find a membership
            }
        }
    }

    //Find the member using the NEA API
    //Pass in the FirstName, LastName, DOB
    //Returns a Member object if found, null if not
    function  CheckForMembership($FirstName, $LastName, $DOB, $SSN4){
        //setup a cURL object to use NEA's API
        $ch = curl_init();
        //set the options
        curl_setopt($ch, CURLOPT_URL, "https://imsQA.nea.org/NEAServices/neaapi/searchIndividual.action");
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disable SSL verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // Get the headers for the token fields from object variables


        //Create an arrary for the fields to post
        $fields = array(
            'lastNm' => urlencode($LastName),
            'firstNm' => urlencode($FirstName),
            'dob' => urlencode($DOB),
            'ssn4' => urldecode($SSN4),
            'appId' => urlencode($this->appId),
            'appTokenId' => urlencode($this->appTokenId),
            //'appUserKey' => urlencode($this->appUserKey)
            'appUserKey' => urlencode("XCOMRFG")
        );

        //url-ify the data
        $fields_string = "";
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string);
        // Set the count of post fields
        curl_setopt($ch, CURLOPT_POST, count($fields));
        // Set the post fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

        //Execute post
        $result = curl_exec($ch);

        // Close the curl object
        curl_close($ch);
        // Decode the JSON result
        return json_decode($result, true);
        /*// Variable to hold results of query
        $message = array();
        // Check to see if there is an error with the token
        if(array_key_exists('TOKENv_ERROR_CODE', $data)) {
            // Re-issue and save the token
            $message['status'] = 'error';
        }
        elseif(empty($data)){
            $message['status'] = 'invalid';
            $message['data'] = $data;
        }
        else {
            $message['status'] = 'okay';
            $message['data'] = $data;
        }
        return $message;*/

    }

    function RetrieveMemberInfo($InaID){
        // Calculate the current membership year. Year = (if current month between September and December, use the next year
        // ) otherwise (if current month between January and August, use the current year) i.e current date = January 2017, m
        // membership year = 2017, if current date = December 2016, membership year = 2017 (2016 + 1)

        // Get the current date information
        $today = getdate();

        // Get the current year
        $year = $today['year'];
        // Get the numeric month
        $month = $today['mon'];

        $membership_date = '';
        if($month >= 1 && $month <= 8){
            // Month between January and August
            $membership_date = $year;
        }
        else {
            // Month between September and December
            $membership_date = $year - 1;
        }

        //setup a cURL object to use NEA's API
        $ch = curl_init();
        //set the options
        curl_setopt($ch, CURLOPT_URL, "https://imsQA.nea.org/NEAServices/neaapi/getMembershipInfo.action");
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disable SSL verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // Set the headers for the token fields
        $fields = array(
            'indvId' => urlencode($InaID),
            'mshpYr' => urlencode($membership_date),
            'appId' => urlencode($this->appId),
            'appTokenId' => urlencode($this->appTokenId),
            //'appUserKey' => urlencode($this->appUserKey)
            'appUserKey' => urlencode("XCOMRFG")
        );

        //url-ify the data
        $fields_string = "";
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string);
        // Set the count of post fields
        curl_setopt($ch, CURLOPT_POST, count($fields));
        // Set the post fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

        //Execute post
        $result = curl_exec($ch);

        // Close the curl object
        curl_close($ch);
        // Decode the JSON result
        return json_decode($result, true);
    }

    function ResetNEAToken(){
        $ch = curl_init();
        //set the options
        curl_setopt($ch, CURLOPT_URL, "https://imsQA.nea.org/NEAServices/usersecurity/requestAPIToken.action");
        // Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disable SSL verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // urlencode the variables to reset the token

        /*$fields = array(
            'appId' => urlencode($this->appId),
            'appUserKey' => urlencode($this->appUserKey),
            'appPwd' => urlencode($this->appPwd)
        );*/
        //*****************DEV CODE DEV CODE DEV CODE*************************
        $fields = array(
            'appId' => urlencode("IMSLITE"),
            'appUserKey' => urlencode("XCOMRFG"),
            'appPwd' => urlencode("z2Uu6!Jf@r")
        );
        //*******************DEV CODE DEV CODE********************************

        $fields_string = "";
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }

        // Set the count of post fields
        curl_setopt($ch, CURLOPT_POST, count($fields));

        // Set the post field data
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

        //Execute post
        $result = curl_exec($ch);

        // Close the curl object
        curl_close($ch);
        // Decode the JSON result
        $data = json_decode($result, true);

        // If the query to NEA was successful, then update the token in the database
        $status = $data['statusCd'];
        $statusMessage = $data['statusMessage'];
        if($status === 0){
            // Get the new token from the JSON array
            $appTokenId = $data['token']['appTokenId'];

            //Perform a database update to store the new token
            $this->SetNEAApiToken($appTokenId);
        }
        else
        {
            // An error occurred, return appropriate error code
            $this->SetErrorMessage(($statusMessage));
            return false;
        }
        // Return data to be evaled by calling function
        return true;

    }
}

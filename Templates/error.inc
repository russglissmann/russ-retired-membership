<!-- Displays error message if someone tries to enroll but the system doesn't find a membership -->
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-10"><h1 class="text-center" id="Header">Retired renewal for Colorado Education Association</h1></div><div class="col"><img src="img/CEAR.png"></div>
    </div>
    <div class="row justify-content-center">
        <div class="sm-col-12"><h2 id="CCError" class="text-danger">We are sorry, but we couldn't locate a retired membership for you. Please click <a href="#" onclick="window.history.back()"> here </a>to return to the previous page.
            Please check your information and submit the form again.</h2></div>
    </div>
</div>
</body>
</html>
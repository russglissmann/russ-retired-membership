<?php
$member = $this->vars["vars"];
?>
<body>
<div class="container">
    <h2 class="text-center">Retired renewal for Colorado Education Association</h2>
    <h3 class="text-center">Thank you for your payment!</h3>
    <p>Your payment of $<?= $member->TotalDues ?> has been processed. Your authorization code is <?= $member->authCode ?>. An email receipt has been sent to <?= $member->HEmail ?>.</p>
    <p>You may close this window and return to the Colorado Education Website.</p>
</body>
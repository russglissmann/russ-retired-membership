<!-- First page to get basic member information to verify membership -->
<body id="Intro">
<div class="container">
    <div class="row">
        <div class="col-sm-10"><h1 class="text-center" id="Header">Retired renewal for Colorado Education Association</h1></div><div class="col"><img src="img/CEAR.png"></div>
    </div>
    <div class="row">
        <div class="col-sm-12"> <h3 class="text-center">Please enter the following information so we can verify your current membership with CEA.</h3></div>
    </div>
    <div class="row">
        <div class="col-sm-12"><h3 class="text-center"><em>PERSONAL INFORMATION</em></h3></div>
    </div>
    <form class="form-horizontal" method="post" id="RetiredRenewalStart" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" >
        <div class="form-group row">
            <label for="FirstName" class="col-sm-2 control-label">First Name <span class="Required">*</span> </label>
            <div class="col-sm-10"><input name="FirstName" id="FirstName" class="form-control" placeholder="First Name" tabindex="1" autofocus></div>
        </div>
        <div class="form-group row">
            <label for="LastName" class="col-sm-2 control-label">Last Name <span class="Required">*</span></label>
            <div class="col-sm-10"><input name="LastName" id="LastName" class="form-control" placeholder="Last Name" tabindex="2" autofocus></div>

        </div>
        <div class="form-group row">
            <label for="BirthDate" class="col-sm-2 control-label">Birth Date <span class="Required">*</span></label>
            <div class="col-sm-10"><input name="BirthDate" id="BirthDate" class="form-control" placeholder="Birth Date" tabindex="3" maxlength="10" autofocus></div>
        </div>
        <div class="form-group row">
            <label for="SSN4" class="col-sm-2 control-label">SSN4</label>
            <div class="col-sm-10"><input name="SSN4" id="SSN4" class="form-control" placeholder="SSN4" tabindex="3" maxlength="10" autofocus></div>
        </div>
        <div class="form-group row justify-content-center">
            <label for=""
            <div class="col-sm-2"><button type="submit" id="IniSubmit" name="Submit" value="SubmitInitial" class="btn btn-lg btn-primary btn-lg" tabindex="4" autofocus  data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Locating your membership">Submit</button></div>
        </div>
    </form>
</div>


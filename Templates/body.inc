<?php
$vars = $this->vars["vars"];
if(count($this->vars["vars"]) > 1){
$member = $vars["Member"];
$Errors = $vars["ErrorVars"];
}
else{
    $member = $this->vars["vars"];
}
?>
<body id="ContactConfirmation" name="ContactConfirmation">
<div class="container">
    <div class="row justify-content-center">
        <div class="sm-col-12"><h2 id="CCError" class="text-danger">DEVELOPMENT VERSION!</h2></div>
    </div>
    <div class="row">
        <div class="col-sm-10"><h1 class="text-center" id="Header">Retired renewal for Colorado Education Association</h1></div><div class="col"><img src="img/CEAR.png"></div>
    </div>
    <div class="row justify-content-center">
        <div class="col"> <h2 class="text-center"><small>Please verify the following information:</small></h2></div>
    </div>
    <div class="row justify-content-center">
        <div class="col"> <h3 class="text-center"><em>PERSONAL INFORMATION</em></h3></div>
    </div>
    <form class="form-horizontal" method="post" id="RetiredRenewal" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" >
        <div class="form-group row">
            <label for="FirstName" class="col-sm-2 control-label">First Name <span class="Required">*</span> </label>
            <div class="col-sm-10"><input type="text" name="FirstName" id="FirstName" class="form-control" placeholder="First Name" tabindex="1" value="<?= $member->FirstName ?>" autofocus></div>
            <?php if(array_key_exists("FirstNameError", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["FirstNameError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <label for="LastName" class="col-sm-2 control-label">Last Name <span class="Required">*</span></label>
            <div class="col-sm-10"><input type="text" name="LastName" id="LastName" class="form-control" placeholder="Last Name" value="<?= $member->LastName ?>" tabindex="2" autofocus></div>
            <?php if(array_key_exists("LastNameError", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["LastNameError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <label for="MI" class="col-sm-2 control-label">Middle Initial</label>
            <div class="col-sm-10"><input type="text" name="MI" id="MI" class="form-control" placeholder="Middle Initial" tabindex="3" maxlength="1" value="<?= $member->MI ?>" autofocus></div>
        </div>
        <div class="form-group row">
            <label for="BirthDate" class="col-sm-2 control-label">Birth Date <span class="Required">*</span></label>
            <div class="col-sm-10"><input type="text" name="DOB" id="DOB" class="form-control" placeholder="Birth Date" tabindex="4" maxlength="10" value="<?= $member->DOB ?>" autofocus></div>
            <?php if(array_key_exists("BirthDateError", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["BirthDateError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <label for="SSN4" class="col-sm-2 control-label">Last Four SSN <span class="Required">*</span></label>
            <div class="col-sm-10"><input type="text" name="SSN4" id="SSN4" class="form-control" placeholder="SSN 4" tabindex="6" maxlength="4" value="<?= $member->SSN4 ?>" autofocus></div>
            <?php if(array_key_exists("SSN4Error", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["SSN4Error"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <label for="HAddress" class="col-sm-2 control-label">Street Address <span class="Required">*</span></label>
            <div class="col-sm-10"><input type="text" name="HAddress" id="HAddress" class="form-control" placeholder="Address" value="<?= $member->HAddress ?>" tabindex="7" autofocus></div>
            <?php if(array_key_exists("AddressError", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["AddressError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <label for="HApt" class="col-sm-2 control-label">Apartment number</label>
            <div class="col-sm-10"><input type="text" name="HApt" id="HApt" class="form-control" placeholder="Apt" value="<?= $member->Apt ?>" tabindex="7" autofocus></div>
            <?php if(array_key_exists("AddressError", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["AddressError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <label for="HCity" class="col-sm-2 control-label">City <span class="Required">*</span></label>
            <div class="col-sm-10"><input type="text" name="HCity" id="HCity" class="form-control" placeholder="City" value="<?= $member->HCity ?>" tabindex="9" autofocus></div>
            <?php if(array_key_exists("CityError", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["CityError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <label for="HZip" class="col-sm-2 control-label">Zip Code <span class="Required">*</span></label>
            <div class="col-sm-10"><input type="text" name="HZip" id="HZip" class="form-control" placeholder="Zip Code" value="<?= $member->HZip ?>" tabindex="10" autofocus></div>
            <?php if(array_key_exists("ZipCodeError", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["ZipCodeError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <label for="HState" class="col-sm-2 control-label">State <span class="Required">*</span></label>
            <div class="col-sm-10"><input type="text" name="HState" id="HState" class="form-control" placeholder="State" maxlength="2" value="<?= $member->HState ?>" tabindex="11" autofocus></div>
            <?php if(array_key_exists("StateError", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["StateError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <label for="MPhone" class="col-sm-2 control-label">Mobile phone <span class="Required">*</span></label>
            <div class="col-sm-10"><input type="tel" name="MPhone" id="MPhone" class="form-control" placeholder="Mobile Phone" value="<?= $member->MPhone ?>" tabindex="12" autofocus></div>
            <?php if(array_key_exists("MPhoneError", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["MPhoneError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <label for="HEmail" class="col-sm-2 control-label">Home email <span class="Required">*</span></label>
            <div class="col-sm-10"><input type="email" name="HEmail" id="HEmail" class="form-control" placeholder="Home Email" value="<?= $member->HEmail ?>" tabindex="13" autofocus></div>
            <?php if(array_key_exists("HomeEmailError", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["HomeEmailError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <div class="col-sm-4">
                <strong>Do you want to change to a Lifetime membership?</strong>
            </div>
            <div class="col-sm">
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="Lifetime" id="LifetimeYes" value="Yes">Yes
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="Lifetime" id="LifetimeNo" value="No">No
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-4">
                <strong>Are you a former ESP member?</strong>
            </div>
            <div class="col-sm">
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="ESP" id="ESPY" value="Yes">Yes
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="ESP" id="ESPN" value="No">No
                    </label>
                </div>
            </div>
        </div>


        <h3 class="text-center"><em>ANNUAL DUES TABLE</em></h3>
        <h4 class="text-center">Dues amount will show when you have answered the last two questions above.</h4>
        <div class="row">
            <div class="col-sm-3 col-sm-offset-3 text-center">Dues Type <span class="Required">*</span></div>
            <div class="col-sm-3 text-center">Dues Amount</div>
        </div>
        <div class="row">
            <div class="col-sm-3 col-sm-offset-3 text-center DuesSurroundLeft">NEA</div>
            <div class="col-sm-3 text-center DuesSurroundRight">$<span id="NEADues">?</span></div>
        </div>
        <div class="row">
            <div class="col-sm-3 col-xs-offset-3 text-center DuesSurroundLeft">CEA</div>
            <div class="col-sm-3 text-center DuesSurroundRight">$<span id="CEADues">?</span> </div>
        </div>
        <?php  if($member->Local != "RETIRED"){ ?>
            <div class="row">
                <div class="col-sm-3 col-sm-offset-3 text-center DuesSurroundLeft" id="LocalName"><?= $member->Local ?></div>
                <div class="col-sm-3 text-center DuesSurroundRight">$<span id="LocalDues">?</span> </div>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-sm-3 col-sm-offset-3 text-center DuesSurroundLeft">Total Dues</div>
            <div class="col-sm-3 text-center DuesSurroundRight">$<span id="SumDues">?</span> </div>
        </div>
        <!-- Credit card payment form -->
        <h3 class="text-center">Pay by credit card</h3>
        <h4 class="text-center">Please enter the following information to pay by credit card:</h4>
        <div class="form-group row">
            <label for="CCFirstName" class="col-sm-2 control-label">First name on card <span class="Required">*</span></label>
            <div class="col-sm-10"><input type="text" id="CCFirstName" name="CCFirstName" class="form-control" value="<?= $member->CCFirstName ?>" tabindex="32"></div>
            <?php if(array_key_exists("CCFirstNameError", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["CCFirstNameError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <label for="CCLastName" class="col-sm-2 control-label">Last name on card <span class="Required">*</span></label>
            <div class="col-sm-10"><input type="text" id="CCLastName" name="CCLastName" class="form-control" value="<?= $member->CCLastName ?>" tabindex="33"></div>
            <?php if(array_key_exists("CCLastNameError", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["CCLastNameError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <label for="CCAddress" class="col-sm-2 control-label">Card address <span class="Required">*</span></label>
            <div class="col-sm-10"><input type="text" id="CCAddress" name="CCAddress" class="form-control" value="<?= $member->CCAddress ?>" tabindex="34"></div>
            <?php if(array_key_exists("CCAddressError", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["CCAddressError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <label for="CCCity" class="col-sm-2 control-label">Card city <span class="Required">*</span></label>
            <div class="col-sm-10"><input type="text" id="CCCity" name="CCCity" class="form-control" value="<?= $member->CCCity ?>" tabindex="35"></div>
            <?php if(array_key_exists("CCCityError", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["CCCVVError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <label for="CCSt" class="col-sm-2 control-label">Card State <span class="Required">*</span></label>
            <div class="col-sm-10"><input type="text" id="CCSt" name="CCSt" maxlength="2" class="form-control" value="<?= $member->CCSt ?>" tabindex="36"></div>
            <?php if(array_key_exists("CCStError", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["CCStError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <label for="CCZip" class="col-sm-2 control-label">Card Zip Code <span class="Required">*</span></label>
            <div class="col-sm-10"><input type="text" id="CCZip" name="CCZip" class="form-control" value="<?= $member->CCZipCode ?>" tabindex="37"></div>
            <?php if(array_key_exists("CCZipError", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["CCZipError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <label for="CCNumber" class="col-sm-2 control-label">Credit Card Number <span class="Required">*</span></label>
            <div class="col-sm-10"><input type="text" id="CCNumber" name="CCNumber" class="form-control" maxlength="16" tabindex="38"> </div>
            <?php if(array_key_exists("CCNumberError", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["CCNumberError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <label for="CCCVV" class="col-sm-2 control-label">CVV Number on back <span class="Required">*</span></label>
            <div class="col-sm-10"><input type="text" id="CCCVV" name="CCCVV" class="form-control" maxlength="4" value="<?= $member->CCCVV ?>" tabindex="39"></div>
            <?php if(array_key_exists("CCCVVError", $Errors)){ print ("<label class=\"col-sm-12 text-center\">*  ". $Errors["CCCVVError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <label for="CCExp" class="col-sm-2 control-label">Expiration Date (MMDD) <span class="Required">*</span></label>
            <div class="col-sm-10"><input type="text" id="CCExp" name="CCExp" class="form-control" maxlength="4" value="<?= $member->CCExp ?>" tabindex="40"></div>
            <?php if(array_key_exists("CCExpError", $Errors)){ print ("<label class=\"col-sm-12 error text-center\">*  ". $Errors["CCExpError"] ."</label>");} ?>
        </div>
        <div class="form-group row">
            <h3 class="col-sm-12 text-center">Amount to be charged: $<span id="sumYearlyDuesCC"></span></h3>
        </div>
        <div class="row justify-content-center">
            <div class="col-sm-8">
                <div class="card">
                    <h3 class="card-header">Member Agreement</h3>
                    <div class="card-block">
                        <p class="card-text">Dues payments are not deductible as charitable contributions for federal income tax purposes. Dues payments (or a portion) may be deducted as a miscellaneous itemized deduction.</p>
                        <p>I authorize payment of dues in the sum determined by my local affiliate (if applicable), the Colorado and National Education Associations.</p>
                        <p class="card-text">This form is for the <span id="MembershipYearSpanStart"></span> - <span id="MembershipYearSpanEnd"></span> membership year: September 1, <span id="MembershipYearStart"></span> through
                        August 31, <span id="MembershipYearEnd"></span> (12 months). </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label for="IAgree" class="col-sm-2 control-label">By checking this box you agree to the above membership terms</label>
            <div class="col-sm-2"><input type="checkbox" id="IAgree" name="IAgree" class="form-control" value="yes" tabindex="41"></div>
            <label class="jserror" style="display: block;"></label>

            <div><label id="CaptchaError" class="jserror" >Captcha error: <?php if(array_key_exists("CaptchaError", $Errors)){echo($Errors["CaptchaError"]);} ?></label></div>
            <div class="col-sm-12 text-center g-recaptcha" data-sitekey="6LfZ6ioUAAAAADA_-TeSgKO-RVeR9gBQFerskghr"></div>
            <div class="col-xs-12 text-center"><button type="submit" id="SubmitForm" name="Submit" value="SubmitForm" tabindex="41"></button> </div>
        </div>
        <div>
        <!-- Hidden variables used for dues -->
            <input type="hidden" id="MemberType" name="MemberType" value="<?= $member->MemberType ?>">
            <input type="hidden" id="LocalID" name="LocalID" value="<?= $member->LocalID ?>">
            <input type="hidden" id="Local" name="Local" value="<?= $member->Local ?>">
            <input type="hidden" id="HINEADues" name="NEADues" value="">
            <input type="hidden" id="HICEADues" name="CEADues" value="">
            <input type="hidden" id="HILocalDues" name="LocalDues" value="">
            <input type="hidden" id="InaID" name="InaID" value="<?= $member->InaID ?>">
            <input type="hidden" id="HIMembershipYearStart" name="HIMembershipYearStart" value="">
            <input type="hidden" id="HIMembershipYearEnd" name="HIMembershipYearEnd" value="">
            <input type="hidden" id="HICCError" name="CCError" value="<?= $member->CCErrorMessage ?>">
            <!-- CC Amount -->
            <input type="hidden" id="HICCAmount" name="HICCAmount" value="">
        </div>
    </form>

</div>
